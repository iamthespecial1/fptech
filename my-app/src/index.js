import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import HTML5Backend from "react-dnd-html5-backend";
import 'bootstrap/dist/css/bootstrap.min.css';
import * as serviceWorker from './serviceWorker';
import { DragDropContext } from "react-dnd";
const ContainerWrapper = DragDropContext(HTML5Backend)(App);
const rootElement = document.getElementById("root");
ReactDOM.render(<ContainerWrapper />, rootElement);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
