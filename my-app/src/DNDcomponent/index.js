import React, { Component } from "react";
import ReactDOM from "react-dom";
import Source from "./source";
import Target from "./target";
import TrainComponent from "../TrainComponent"
class Container extends Component {
  constructor() {
    super();
    this.state = {
      droppedItem: {}
    };
    this.onDrop = this.onDrop.bind(this);
  }

  onDrop(item) {
    this.setState({
      droppedItem: item
    });
  }

  render() {
    return (
      <div className="App">
        <div className="source">
          <Source name="Carriage" id="C" />
          <Source name="Locomotive" id="L" />
        </div>
        <TrainComponent />
      </div>
    );
  }
}

export default Container