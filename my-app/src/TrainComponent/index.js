import React, { Component } from "react";
import Target from "../DNDcomponent/target";
import TrainList from "./trainList"
import { validateTrain } from "./helper"
class TrainComponent extends Component {
    constructor() {
        super();
        this.state = {
            droppedItem: [],
            train: null
        };
        this.onDrop = this.onDrop.bind(this);
    }

    onDrop(item) {
        this.setState(prevState => ({
            droppedItem: [...prevState.droppedItem, item]
        }));
    }
    handleCreate = () => {
        let validate = validateTrain(this.state.droppedItem)
        if (validate.flag) {
            alert(validate.message)
        }
        else {
            this.setState(prevState => ({
                droppedItem: [],
                train: prevState.droppedItem
            }))
        }
    }
    handleCancelTrain = () => {
        this.setState(prevState => ({
            droppedItem: []
        }))
    }

    render() {
        return (
            <div className="row">
                <div className="destination">
                    <Target
                        droppedItem={this.state.droppedItem}
                        onDrop={this.onDrop}
                        handleCreate={this.handleCreate}
                        handleCancelTrain={this.handleCancelTrain} />
                </div>
                <div>
                    <TrainList train={this.state.train} />
                </div>
            </div>
        );
    }
}

export default TrainComponent