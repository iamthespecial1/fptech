export const trainfinder = (timings, index, name, value) => {
    let mock = JSON.parse(JSON.stringify(timings))
    if (name === "departure") {
        let obj = timings[index]
        obj = { ...obj, departure: value }
        mock[index] = obj
    }
    else if (name === "arrival") {
        let obj = timings[index]
        obj = { ...obj, arrival: value }
        mock[index] = obj
    }
    return mock
}

export const findnumberofPlatforms = (timings) => {
    let mock = JSON.parse(JSON.stringify(timings))
    let arrival = []
    let departure = []
    mock.forEach((val) => {
        let arr = val.arrival === ""||!val.arrival ? 0 : parseFloat(val.arrival)
        arrival.push(arr)
        let dep = val.departure === ""||!val.arrival ? 0 : parseFloat(val.departure)
        departure.push(dep)
    })
    let sortedArrival = arrival.sort((a, b) => {
        return a - b
    })
    let sortedDeparture = departure.sort((a, b) => {
        return a-b
    })
    console.log(sortedArrival)
    console.log(sortedDeparture)
    let plat_needed = 1;
    let result = 1;
    let i = 1;
    let j = 0;
    while (i < timings.length && j < timings.length) {
        if (sortedArrival[i] <= sortedDeparture[j]) {
            plat_needed++;
            i++;
            if (plat_needed > result)
                result = plat_needed;
        }
        else {
            plat_needed--;
            j++;
        }
    }

    return result;
}

export const drawTrain = (train) => {
    let s = ""
    train.forEach(element => {
        s += element.id + " - "
    });
    return s
}

export const validateTrain = (train) => {
    if (checkForLocoCount(train)) {
        return { message: "More than 1 Loco", flag: true }
    }
    if (!checkForLocomotive(train)) {
        return { message: "Locomotive Missing", flag: true }
    }
    if (!checkForCarriage(train)) {
        return { message: "Carriage Missing", flag: true }
    }
    return { message: "", flag: false }
}

const checkForLocomotive = (train) => {
    let flag = false;
    train.forEach((val) => {
        if (val.id === "L") {
            flag = true;
        }
    })
    return flag
}
const checkForCarriage = (train) => {
    let flag = false;
    train.forEach((val) => {
        if (val.id === "C") {
            flag = true;
        }
    })
    return flag
}

const checkForLocoCount = (train) => {
    let count = 0;
    train.forEach((val) => {
        if (val.id === "L") {
            count++
        }
    })
    if (count > 1)
        return true
    else
        return false
}