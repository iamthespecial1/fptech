import React, { Component } from "react";
import Target from "../DNDcomponent/target";
import './trainList.css'
import { trainfinder, findnumberofPlatforms, drawTrain } from "./helper"
class TrainList extends Component {
    constructor() {
        super();
        this.state = {
            resArray: [],
            timings: []
        };
    }

    componentDidUpdate(prevProps) {
        if (this.props.train !== prevProps.train) {
            this.setState(prevState => ({
                resArray: [...prevState.resArray, this.props.train],
                timings: [...prevState.timings, { arrival: "", departure: "" }]
            }))
        }
    }

    handleChange = (e, index) => {
        let newTimings = trainfinder(this.state.timings, index, e.target.name, e.target.value)
        this.setState(prevState => ({
            timings: newTimings
        }))
    }

    handleCalculate = () => {
        alert("Number of platforms required= " + findnumberofPlatforms(this.state.timings))
    }


    render() {
        const { resArray, timings } = this.state
        return (
            <>
                <div className="m-3 trainList row">
                    {resArray.map((val, index) => <div key={index} className="d-flex border m-3 ">
                        <div className="col-md-6">{drawTrain(val)}</div>
                        <input className="col-md-3" placeholder="Arrival (HH.MM)" name="arrival" onChange={(e) => this.handleChange(e, index)} value={this.state.timings[index].arrival} />
                        <input className="col-md-3" placeholder="Dep (HH.MM)" name="departure" onChange={(e) => this.handleChange(e, index)} value={this.state.timings[index].departure} />
                    </div>)}

                </div>
                <div>
                    {this.state.resArray.length ? <button onClick={this.handleCalculate}>Calculate</button> : null}
                </div>

            </>
        );
    }
}

export default TrainList